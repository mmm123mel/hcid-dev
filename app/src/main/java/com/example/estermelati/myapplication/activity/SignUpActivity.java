package com.example.estermelati.myapplication.activity;

import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.RestApi;
import com.example.estermelati.myapplication.fragment.HomeFragment;
import com.example.estermelati.myapplication.fragment.VerifyOTPFragment;
import com.example.estermelati.myapplication.model.Device;
import com.example.estermelati.myapplication.model.RegisterModel;
import com.example.estermelati.myapplication.model.UserModel;
import com.example.estermelati.myapplication.util.Utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ester.melati on 15/06/2017.
 */

public class SignUpActivity extends AppCompatActivity {
    RegisterModel registerModel;
    Device device;
    private static View view;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.btnBack)
    TextView btnBack;
    @BindView(R.id.buatAkun)
    TextView buatAkun;
    @BindView(R.id.tv_lanjut)
    TextView tv_lanjut;
    @BindView(R.id.tv_hintBirthdate)
    TextView tv_hintBirthdate;
    @BindView(R.id.tv_birthdate)
    TextView tv_birthdate;
    @BindView(R.id.tv_hintHP)
    TextView tv_hintHP;
    @BindView(R.id.et_userId)
    EditText et_userId;
    @BindView(R.id.et_PIN)
    EditText et_PIN;
    @BindView(R.id.et_PINConfirmation)
    EditText et_PINConfirmation;
    @BindView(R.id.cb_termsConditions)
    CheckBox cb_termsConditions;
    @BindView(R.id.tv_agreeTNC)
    TextView tv_agreeTNC;
    String otpId, otpCode, email, userId, phone, pin, loginType, dateOfBirth, imei, deviceId,
    osVersion, model, manufacturer, serialNumber, career, ram, totalDiskMemory, availableDiskMemory;
    ProgressDialog progressDialog;
    Calendar calendar;
    FragmentManager fragmentManager;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        calendar = Calendar.getInstance();
        tv_agreeTNC.setText(Html.fromHtml("Saya setuju dengan " + "<b><a href=\"https://www.homecredit.co.id/Term-and-Condition\">Ketentuan Penggunaan dan Kebijakan Privasi aplikasi My Home Credit</a></b>"));
        tv_agreeTNC.setMovementMethod(LinkMovementMethod.getInstance());
        progressDialog = new ProgressDialog(SignUpActivity.this);
    }

    @OnClick(R.id.btnBack) void replaceLogin(){
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tv_lanjut) void checkValidation() {
//        check if all strings are null of not
        if(et_userId.equals("") || et_userId.length() == 0 ||
                et_PIN.equals("") || et_PIN.length() == 0 ||
                et_PINConfirmation.equals("") || et_PINConfirmation.length() == 0){
            Toast.makeText(getApplicationContext(), "Isi semua field.", Toast.LENGTH_SHORT).show();
        }
//        check if both password should be equal
        else if (!et_PINConfirmation.equals(et_PIN)){
            Toast.makeText(getApplicationContext(), "PIN tidak sama", Toast.LENGTH_SHORT).show();
        }
//        make sure user check the T&C checkbox
        else if(!cb_termsConditions.isChecked()){
            Toast.makeText(getApplicationContext(), "Silakan centang Syarat dan Ketentuan", Toast.LENGTH_SHORT).show();
        }
//        else, sign up
        else {
//            register();
            prepareDataToSubmit();
            startVerifyOTPFragment();
        }
    }

    @OnClick(R.id.tv_birthdate) void showDatePicker(){
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            }
        };
        new DatePickerDialog(SignUpActivity.this, date, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
        updateLabel();
    }

    private void updateLabel() {
        String birthDateFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(birthDateFormat, Locale.UK);
        tv_birthdate.setText(sdf.format(calendar.getTime()));
    }

    private void register() {
        registerModel = new RegisterModel();
        prepareDataToSubmit();
        device = new Device();
        device.setImei(imei);
        device.setOsVersion(osVersion);
        device.setModel(model);
        device.setManufacturer(manufacturer);
        device.setSerialNumber(serialNumber);
        device.setCareer(career);
        device.setRam(ram);
        device.setTotalDiskMemory(totalDiskMemory);
        device.setAvailableDiskMemory(availableDiskMemory);
        device.setUserId(userId);
        registerModel.setOtpId(otpId);
        registerModel.setOtpCode(otpCode);
        registerModel.setEmail(email);
        registerModel.setUserId(userId);
        registerModel.setPhone(phone);
        registerModel.setPin(pin);
        registerModel.setLoginType(loginType);
        registerModel.setDateOfBirth(dateOfBirth);
        registerModel.setDeviceId(deviceId);
        registerModel.setDevice(device);

        RestApi restApi = RestApi.retrofit.create(RestApi.class);
        Call<RegisterModel> call = restApi.register(registerModel);
        call.enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                registerModel = response.body();
                String status = registerModel.getStatus();
                String message = registerModel.getMessage();
                progressDialog.dismiss();
                Intent intent = new Intent(SignUpActivity.this, IntroductionActivity.class);
                startActivity(intent);
                Log.d("RegisterActivity", status);
                Toast.makeText(getApplicationContext(), "Hai, " + userId + " - " + status + " - " + message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                Log.e("ERROR", t.getMessage());
                Toast.makeText(SignUpActivity.this, "Gagal login", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void prepareDataToSubmit() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imei = telephonyManager.getDeviceId();
        userId = et_userId.getText().toString();
        phone = userId;
        pin = et_PIN.getText().toString();
        loginType = "MANUAL";
        dateOfBirth = tv_birthdate.getText().toString();
        osVersion = Build.VERSION.RELEASE;
        manufacturer = Build.MANUFACTURER;
        model = Build.MODEL;
        serialNumber = Build.SERIAL;
        career = telephonyManager.getNetworkOperatorName();
        ram = String.valueOf(totalRamMemorySize());
        totalDiskMemory =  totalInternalMemorySize();
        availableDiskMemory = availableInternalMemorySize();
        returnData();
    }

    public void returnData() {
        setUserId();
        setPhone();
        setPin();
        setLoginType();
        setDateOfBirth();
        setImei();
        setOsVersion();
        setModel();
        setManufacturer();
        setSerialNumber();
        setCarrier();
        setRam();
        setTotalDiskMemory();
        setAvailableDiskMemory();
    }

    String setAvailableDiskMemory() {
        return availableDiskMemory;
    }

    String setTotalDiskMemory() {
        return totalDiskMemory;
    }

    String setRam() {
        return ram;
    }

    String setCarrier() {
        return career;
    }

    String setSerialNumber() {
        return serialNumber;
    }

    String setManufacturer() {
        return manufacturer;
    }

    String setModel() {
        return model;
    }

    String setOsVersion() {
        return osVersion;
    }

    String setImei() {
        return imei;
    }

    String setDateOfBirth() {
        return dateOfBirth;
    }

    String setLoginType() {
        return loginType;
    }

    String setPin() {
        return pin;
    }

    String setPhone() {
        return phone;
    }

    String setUserId() {
        return userId;
    }

    public void startVerifyOTPFragment(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new VerifyOTPFragment())
                .addToBackStack("f_verifyOTP").commit();
    }

    private long totalRamMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.totalMem / 1048576L;
        return availableMegs;
    }

    String availableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return formatSize(availableBlocks * blockSize);
    }

    String totalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return formatSize(totalBlocks * blockSize);
    }

    public static String formatSize(long size) {
        String suffix = null;
        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }
}
