package com.example.estermelati.myapplication.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "fullName",
        "mothersName",
        "phone",
        "email",
        "birthdate",
        "otherPhoneNumber",
        "permanentAddress",
        "contactAddress",
        "bankAccount"
})
public class PersonalDetailsModel {

    @JsonProperty("fullName")
    private String fullName;
    @JsonProperty("mothersName")
    private String mothersName;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("email")
    private String email;
    @JsonProperty("birthdate")
    private String birthdate;
    @JsonProperty("otherPhoneNumber")
    private String otherPhoneNumber;
    @JsonProperty("permanentAddress")
    private String permanentAddress;
    @JsonProperty("contactAddress")
    private String contactAddress;
    @JsonProperty("bankAccount")
    private String bankAccount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("fullName")
    public String getFullname() {
        return fullName;
    }

    @JsonProperty("fullName")
    public void setFullname(String fullName) {
        this.fullName = fullName;
    }

    @JsonProperty("mothersName")
    public String getMotherName() {
        return mothersName;
    }

    @JsonProperty("mothersName")
    public void setMotherName(String mothersName) {
        this.mothersName = mothersName;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("birthdate")
    public String getBirthdate() {
        return birthdate;
    }

    @JsonProperty("birthdate")
    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    @JsonProperty("otherPhoneNumber")
    public String getOtherPhoneNumber() {
        return otherPhoneNumber;
    }

    @JsonProperty("otherPhoneNumber")
    public void setOtherPhoneNumber(String otherPhoneNumber) {
        this.otherPhoneNumber = otherPhoneNumber;
    }

    @JsonProperty("permanentAddress")
    public String getPermanentAddress() {
        return permanentAddress;
    }

    @JsonProperty("permanentAddress")
    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    @JsonProperty("contactAddress")
    public String getContactAddress() {
        return contactAddress;
    }

    @JsonProperty("contactAddress")
    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    @JsonProperty("bankAccount")
    public String getBankAccount() {
        return bankAccount;
    }

    @JsonProperty("bankAccount")
    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}