package com.example.estermelati.myapplication.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "otpId",
        "otpCode",
        "email",
        "userId",
        "phone",
        "pin",
        "loginType",
        "dateOfBirth",
        "status",
        "message",
        "token",
        "deviceId",
        "device"
})
public class RegisterModel {

    @JsonProperty("otpId")
    private String otpId;
    @JsonProperty("otpCode")
    private String otpCode;
    @JsonProperty("email")
    private String email;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("pin")
    private String pin;
    @JsonProperty("loginType")
    private String loginType;
    @JsonProperty("dateOfBirth")
    private String dateOfBirth;
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("token")
    private Token token;
    @JsonProperty("device")
    private Device device;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("otpId")
    public String getOtpId() {
        return otpId;
    }

    @JsonProperty("otpId")
    public void setOtpId(String otpId) {
        this.otpId = otpId;
    }

    @JsonProperty("otpCode")
    public String getOtpCode() {
        return otpCode;
    }

    @JsonProperty("otpCode")
    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("pin")
    public String getPin() {
        return pin;
    }

    @JsonProperty("pin")
    public void setPin(String pin) {
        this.pin = pin;
    }

    @JsonProperty("loginType")
    public String getLoginType() {
        return loginType;
    }

    @JsonProperty("loginType")
    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    @JsonProperty("dateOfBirth")
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @JsonProperty("dateOfBirth")
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("token")
    public Token getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(Token token) {
        this.token = token;
    }

    @JsonProperty("device")
    public Device getDevice() {
        return device;
    }

    @JsonProperty("device")
    public void setDevice(Device device) {
        this.device = device;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}