package com.example.estermelati.myapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.RestApi;
import com.example.estermelati.myapplication.model.HomePageModel;
import com.example.estermelati.myapplication.model.Token;
import com.example.estermelati.myapplication.model.UserModel;
import com.example.estermelati.myapplication.util.Utils;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ester.melati on 15/06/2017.
 */

public class LoginActivity extends AppCompatActivity {
    private static View view;
    private static Animation shakeAnimation;
    private FragmentManager fragmentManager;
    @BindView(R.id.loginBtn)
    Button loginButton;
    @BindView(R.id.lupaPIN)
    TextView lupaPIN;
    @BindView(R.id.tv_userId)
    TextView tv_userId;
    @BindView(R.id.et_userId)
    EditText et_userId;
    @BindView(R.id.et_PIN)
    EditText et_PIN;
    @BindView(R.id.createAccount)
    TextView signUp;
    @BindView(R.id.registerNow)
    TextView registerNow;
    @BindView(R.id.tv_hintPIN)
    TextView tv_hintPIN;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    String userId, pin, loginType, status, username, message, aT, deviceId, refreshToken, tokenExpired, tokenCreated;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    UserModel userModel;
    int expired;
    boolean isDummy;
    private static final String USERID = "USERID";
    private static final String IS_LOGGED_IN = "IS_LOGGED_IN";
    LinearLayout loginForm, pinlockForm;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        ButterKnife.bind(this);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();
        progressDialog = new ProgressDialog(LoginActivity.this);
    }

    private void checkLoginStatus() {
        getUserAccount();
        if(getUserAccount().equals("")){
            loadLoginLayout();
        } else {
            loadPinLock();
        }
    }

    private void loadPinLock() {
        pinlockForm = (LinearLayout)this.findViewById(R.id.ll_pin_lock);
        pinlockForm.setVisibility(LinearLayout.VISIBLE);
    }

    private void loadLoginLayout() {
        loginForm = (LinearLayout)this.findViewById(R.id.loginForm);
        loginForm.setVisibility(LinearLayout.VISIBLE);
    }

    @OnClick(R.id.registerNow) void createAccount(){
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.lupaPIN) void forgotPassword(){
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.loginBtn) void checkValidation() {
        userId = et_userId.getText().toString();
        pin = et_PIN.getText().toString();

        if(userId.equals("") || userId.length() == 0
                || pin.equals("") || pin.length() == 0){
            Toast.makeText(getApplicationContext(), "Enter both credential.", Toast.LENGTH_SHORT).show();
        } else {
            preferences = getSharedPreferences(USERID, MODE_PRIVATE);
            editor = preferences.edit();
            editor.putString(USERID, userId);
            editor.putBoolean(IS_LOGGED_IN, true);
            editor.commit();
            login();
        }
    }

    public HashMap<String, String> getUserAccount(){
        HashMap<String, String> USER = new HashMap<String, String>();
        USER.put(USERID, preferences.getString(USERID, null));
        Log.d("PREFERENCES", USER.toString());
        return USER;
    }

    public void login(){
        userModel = new UserModel();
        loginType = "MANUAL";
        userModel.setUserId(userId);
        userModel.setPin(pin);
        userModel.setLoginType(loginType);
        progressDialog.show();
        progressDialog.setMessage("Mohon tunggu...");
        progressDialog.setCancelable(false);
        RestApi restApi = RestApi.retrofit.create(RestApi.class);
        Call<UserModel> call = restApi.login(userModel);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                userModel = response.body();
                Log.d("LoginActivity", "pesan: " + response.message() + " - " + response.code() + " - " + response.errorBody());
                status = userModel.getStatus();
                Log.d("LoginActivity", "status: " + status);
                message = userModel.getMessage();
                if(status.equals("FAILED")) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    et_userId.setText("");
                    et_PIN.setText("");
                } if (status.equals("SUCCESS")){
                    username = userModel.getToken().getUsername();
                    aT = userModel.getToken().getAccessToken();
                    refreshToken = userModel.getToken().getRefreshToken();
                    expired = userModel.getToken().getExpired();
                    tokenCreated = userModel.getToken().getTokenCreated();
                    tokenExpired = userModel.getToken().getTokenExpired();
                    isDummy = userModel.getToken().isDummy();
                    Log.d("LoginActivity", "username: " + username);
                    Log.d("LoginActivity", "aT: " + aT);
                    progressDialog.dismiss();
                    Intent main = new Intent(LoginActivity.this, IntroductionActivity.class);
                    main.putExtra("accessToken", aT);
                    startActivity(main);
//                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("ERROR", t.getMessage());
                Toast.makeText(LoginActivity.this, "Periksa koneksi Internet Anda", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
