package com.example.estermelati.myapplication.model;

/**
 * Created by ester.melati on 27/06/2017.
 */

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "red",
        "orange",
        "green"
})
public class ThemeColorModel {

    @JsonProperty("red")
    private int red;
    @JsonProperty("orange")
    private int orange;
    @JsonProperty("green")
    private int green;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("red")
    public int getRed() {
        return red;
    }

    @JsonProperty("red")
    public void setRed(int red) {
        this.red = red;
    }

    @JsonProperty("orange")
    public int getOrange() {
        return orange;
    }

    @JsonProperty("orange")
    public void setOrange(int orange) {
        this.orange = orange;
    }

    @JsonProperty("green")
    public int getGreen() {
        return green;
    }

    @JsonProperty("green")
    public void setGreen(int green) {
        this.green = green;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}