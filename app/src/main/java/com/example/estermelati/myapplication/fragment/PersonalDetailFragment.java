package com.example.estermelati.myapplication.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.activity.IntroductionActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonalDetailFragment extends Fragment {
    @BindView(R.id.tv_fullName)
    TextView tv_fullName;
    @BindView(R.id.tv_motherName)
    TextView tv_motherName;
    @BindView(R.id.tv_phone)
    TextView tv_phone;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.tv_birthdate)
    TextView tv_birthdate;
    @BindView(R.id.tv_otherPhoneNumber)
    TextView tv_otherPhoneNumber;
    @BindView(R.id.tv_permanentAddress)
    TextView tv_permanentAddress;
    private static View view;
    private FragmentManager fragmentManager;

    String fullname, motherName, phone, email, birthdate, otherPhoneNumber,
        permanentAddress, contactAddress, bankAccount;

    public PersonalDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_personal_detail, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        ButterKnife.bind(this, view);
        fragmentManager = getActivity().getSupportFragmentManager();
        retrieveData();
        tv_fullName.setText(fullname);
        tv_motherName.setText(motherName);
        tv_phone.setText(phone);
        tv_email.setText(email);
        tv_birthdate.setText(birthdate);
        tv_otherPhoneNumber.setText(otherPhoneNumber);
        tv_permanentAddress.setText(permanentAddress);
    }

    private void retrieveData() {
        IntroductionActivity activity = (IntroductionActivity) getActivity();
        fullname = activity.preparePdFullname();
        motherName = activity.preparePdMotherName();
        phone = activity.preparePdPhone();
        email = activity.preparePdEmail();
        birthdate = activity.preparePdBirthday();
        otherPhoneNumber = activity.preparePdOtherPhoneNumber();
        permanentAddress = activity.preparePdPermanentAddress();
        contactAddress = activity.preparePdContactAddress();
        bankAccount = activity.preparePdBankAccount();
    }
}
