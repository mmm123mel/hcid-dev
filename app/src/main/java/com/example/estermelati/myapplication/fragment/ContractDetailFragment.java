package com.example.estermelati.myapplication.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.activity.IntroductionActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ester.melati on 07/07/2017.
 */

public class ContractDetailFragment extends Fragment {
    @BindView(R.id.tag_contractNo)
    TextView tag_contractNo;
    @BindView(R.id.tag_agentName)
    TextView tag_agentName;
    @BindView(R.id.tv_contractNo)
    TextView tv_contractNo;
    @BindView(R.id.tv_contractDate)
    TextView tv_contractDate;
    @BindView(R.id.tv_prodName)
    TextView tv_prodName;
    @BindView(R.id.tv_price)
    TextView tv_price;
    @BindView(R.id.tv_payDateRecommendation)
    TextView tv_payDateRecommendation;
    @BindView(R.id.tag_productDetail)
    TextView tag_productDetail;

    FragmentManager fragmentManager;
    private static View view;

    String agentName, contractNo, contractDate, productName, price, payDateRecommendation;

    public ContractDetailFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = inflater.inflate(R.layout.contract_detail_layout, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        ButterKnife.bind(this, view);
        fragmentManager = getActivity().getSupportFragmentManager();
        retrieveData();
        tag_agentName.setText(agentName);
        tv_contractNo.setText(contractNo);
        tv_contractDate.setText(contractDate);
        tv_prodName.setText(productName);
        tv_price.setText(price);
        tv_payDateRecommendation.setText(payDateRecommendation);
    }

    private void retrieveData() {
        IntroductionActivity activity = (IntroductionActivity) getActivity();
    }
}
