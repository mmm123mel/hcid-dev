package com.example.estermelati.myapplication.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.RestApi;
import com.example.estermelati.myapplication.activity.IntroductionActivity;
import com.example.estermelati.myapplication.adapter.HomePagerAdapter;
import com.example.estermelati.myapplication.model.ContractHistoryModel;
import com.example.estermelati.myapplication.model.HomePageModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ester.melati on 16/06/2017.
 */

public class HomeFragment extends Fragment {

    private static View view;
    private FragmentManager fragmentManager;
    @BindView(R.id.pager_tab1)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.tv_lokasi_pembayaran)
    TextView lokasi_pembayaran;
    @BindView(R.id.tv_cara_membayar)
    TextView cara_membayar;
    @BindView(R.id.tv_payDate)
    TextView tv_payDate;
//    @BindView(R.id.tv_toolbarTitle)
//    TextView tv_toolbarTitle;
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    ProgressBar progressBar;
    HomePageModel homePageModel;
    String versionCode, contentType, accessToken, status, name, message, email_hp, updateLink,
            aboutUs, phone, contactUs_email, contactUs_phone, contactUs_fax, aToken,
            contractHistories_status, contractHistories_message,
            contractHistory_contractNumber, contractHistory_status;
    boolean eligibility, existingCustomer;
    int themeColor_red, themeColor_orange, themeColor_green;
    HomePagerAdapter homePagerAdapter;
    ArrayList<ContractHistoryModel> contrHistoryData = new ArrayList<>();

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_home, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        initViews();
        return view;
    }

    private void initViews() {
        ButterKnife.bind(this, view);
        fragmentManager = getActivity().getSupportFragmentManager();
        retrieveData();
        loadHome();
    }

    private void setViewPager(ViewPager viewPager) {
        if(homePagerAdapter == null){
            homePagerAdapter = new HomePagerAdapter(getActivity(), contrHistoryData);
            viewPager.setAdapter(homePagerAdapter);
            Log.d("HomeFragment", "new adapter has set...");
        } else {
            homePagerAdapter.notifyDataSetChanged();
            Log.d("HomeFragment", "notification for dataset changes has set...");
        }
    }

    @OnClick(R.id.tv_cara_membayar) void loadCaraBayar(){
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new CaraBayarCicilanFragment())
                .addToBackStack(null).commit();
//        tv_toolbarTitle.setText("Cara Pembayaran");
//        toolbar.setBackgroundColor(Color.RED);
    }

    @OnClick(R.id.tv_lokasi_pembayaran) void loadLokasiPembayaran(){
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new LokasiPembayaranFragment())
                .addToBackStack(null).commit();
//        tv_toolbarTitle.setText("Lokasi Pembayaran");
//        toolbar.setBackgroundColor(Color.RED);
    }

    @OnClick(R.id.tv_payDate) void loadJadwalPembayaran(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new JadwalPembayaranFragment())
                .addToBackStack(null).commit();
//        tv_toolbarTitle.setText("Jadwal Pembayaran");
//        toolbar.setBackgroundColor(Color.RED);
    }

    public void loadHome(){
        homePageModel = new HomePageModel();
        versionCode = "1";
        contentType = "application/json";
        accessToken = aToken;
        Log.d("accessToken1", accessToken);
        homePageModel.setVersionCode(versionCode);
        progressBar.setVisibility(View.VISIBLE);
        RestApi restApi = RestApi.retrofit.create(RestApi.class);
        Call<HomePageModel> call = restApi.home(accessToken, contentType, homePageModel);
        call.enqueue(new Callback<HomePageModel>() {
            @Override
            public void onResponse(Call<HomePageModel> call, Response<HomePageModel> response) {
                homePageModel = response.body();
                status = homePageModel.getStatus();
                message = homePageModel.getMessage();
                name = homePageModel.getName();
                email_hp = homePageModel.getEmail();
                updateLink = homePageModel.getUpdateLink();
                aboutUs = homePageModel.getAboutUs();
                phone = homePageModel.getPhone();
                eligibility = homePageModel.isEligibility();
                existingCustomer = homePageModel.isExistingCustomer();
                contactUs_email = homePageModel.getContactUs().getEmail();
                contactUs_phone = homePageModel.getContactUs().getPhone();
                contactUs_fax = homePageModel.getContactUs().getFax();
                themeColor_red = homePageModel.getThemeColor().getRed();
                themeColor_orange = homePageModel.getThemeColor().getOrange();
                themeColor_green = homePageModel.getThemeColor().getGreen();
                contractHistories_status = homePageModel.getContractHistories().getStatus();
                contractHistories_message = homePageModel.getContractHistories().getMessage();
                contrHistoryData.addAll(homePageModel.getContractHistories().getContractHistory());
                Log.d("HomeFragment", "status: " + status);
                Log.d("HomeFragment", "message: " + message);
                Log.d("HomeFragment", "email_hp: " + email_hp);
                Log.d("HomeFragment", "updateLink: " + updateLink);
                Log.d("HomeFragment", "aboutUs: " + aboutUs);
                Log.d("HomeFragment", "phone: " + phone);
                Log.d("HomeFragment", "eligibility: " + eligibility);
                Log.d("HomeFragment", "existingCustomer: " + existingCustomer);
                Log.d("HomeFragment", "contactUs_email: " + contactUs_email);
                Log.d("HomeFragment", "contactUs_phone: " + contactUs_phone);
                Log.d("HomeFragment", "contactUs_fax: " + contactUs_fax);
                Log.d("HomeFragment", "contractHistory_contractNumber: " + contractHistory_contractNumber);
                Log.d("HomeFragment", "contractHistory_status: " + contractHistory_status);
                progressBar.setVisibility(View.GONE);
                setViewPager(viewPager);
                tabLayout.setupWithViewPager(viewPager);
            }
            @Override
            public void onFailure(Call<HomePageModel> call, Throwable t) {
                Log.d("Home onFailure", t.getMessage() + " - " + t.getLocalizedMessage()
                        + " - " + t.getStackTrace());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void retrieveData() {
        IntroductionActivity introActivity = (IntroductionActivity) getActivity();
        aToken = introActivity.prepareAccessToken();
        Log.d("accessToken2", aToken);
    }

}
