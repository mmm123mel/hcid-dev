package com.example.estermelati.myapplication;

import com.example.estermelati.myapplication.model.HomePageModel;
import com.example.estermelati.myapplication.model.RegisterModel;
import com.example.estermelati.myapplication.model.UserModel;
import com.example.estermelati.myapplication.response.PersonalDetailResponse;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by ester.melati on 22/06/2017.
 */

public interface RestApi {

    String URL = "http://sandbox.homecredit.co.id/portal-api-r21/";

    @POST("login")
    Call<UserModel> login(@Body UserModel userModel);

    @POST("register")
    Call<RegisterModel> register(@Body RegisterModel registerModel);

    @POST("home")
    Call<HomePageModel> home(@Header("Authorization") String accessToken,
                             @Header("Content-Type") String contentType,
                             @Body HomePageModel homePageModel);

    @GET("getPersonalDetails")
    Call<PersonalDetailResponse> getPersonalDetails(@Header("Authorization") String accessToken,
                                                    @Header("X-API-Version") String apiVersion);

    OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
