package com.example.estermelati.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.util.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ester.melati on 15/06/2017.
 */

public class ForgotPasswordActivity extends AppCompatActivity {
    @BindView(R.id.forgot_button) TextView submit;
    @BindView(R.id.backToLoginBtn) TextView back;

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpassword_layout);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.backToLoginBtn) void replaceLogin(){
        Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.forgot_button) void submitButtonTask() {
//      Check if email is not null
//        if(getEmailID.equals("") || getEmailID.length() == 0){
//            Toast.makeText(getApplicationContext(), "Please enter your email ID.", Toast.LENGTH_SHORT).show();
//        }
//
////        check if email id is valid or not
//        else if(!m.find()){
//            Toast.makeText(getApplicationContext(), "Your email ID is invalid.", Toast.LENGTH_SHORT).show();
//        }
//
//        else {
//            Toast.makeText(getApplicationContext(), "Get forgot password.", Toast.LENGTH_SHORT).show();
//        }
    }
}
