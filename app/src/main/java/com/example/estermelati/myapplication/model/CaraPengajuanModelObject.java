package com.example.estermelati.myapplication.model;

import com.example.estermelati.myapplication.R;

/**
 * Created by ester.melati on 22/06/2017.
 */

public enum CaraPengajuanModelObject {
    T1(R.layout.view_tahap1),
    T2(R.layout.view_tahap2),
    T3(R.layout.view_tahap3),
    T4(R.layout.view_tahap4),
    T5(R.layout.view_tahap5),
    T6(R.layout.view_tahap6);

    private int mLayoutResId;

    CaraPengajuanModelObject(int layoutResId){
        mLayoutResId = layoutResId;
    }

    public int getLayoutResId(){
        return mLayoutResId;
    }
}
