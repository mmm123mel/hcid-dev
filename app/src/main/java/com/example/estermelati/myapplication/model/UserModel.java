package com.example.estermelati.myapplication.model;

import java.util.HashMap;
import java.util.Map;

public class UserModel {

    private String userId;
    private String pin;
    private String loginType;
    private String deviceId;
    private String status;
    private String message;
    private Token token;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public UserModel() {
    }

    /**
     *
     * @param message
     * @param token
     * @param status
     * @param pin
     * @param userId
     * @param loginType
     * @param deviceId
     */
    public UserModel(String userId, String pin, String loginType, String deviceId, String status, String message, Token token) {
        super();
        this.userId = userId;
        this.pin = pin;
        this.loginType = loginType;
        this.deviceId = deviceId;
        this.status = status;
        this.message = message;
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}