package com.example.estermelati.myapplication.model;

/**
 * Created by ester.melati on 27/06/2017.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "eid",
        "version",
        "createdDate",
        "lastModifiedDate",
        "contractNumber",
        "commodityCategory",
        "commodityType",
        "brand",
        "commodityPrice"
})
public class CommodityList {

    @JsonProperty("eid")
    private String eid;
    @JsonProperty("version")
    private String version;
    @JsonProperty("createdDate")
    private String createdDate;
    @JsonProperty("lastModifiedDate")
    private String lastModifiedDate;
    @JsonProperty("contractNumber")
    private String contractNumber;
    @JsonProperty("commodityCategory")
    private String commodityCategory;
    @JsonProperty("commodityType")
    private String commodityType;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("commodityPrice")
    private int commodityPrice;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("eid")
    public String getEid() {
        return eid;
    }

    @JsonProperty("eid")
    public void setEid(String eid) {
        this.eid = eid;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("createdDate")
    public String getCreatedDate() {
        return createdDate;
    }

    @JsonProperty("createdDate")
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @JsonProperty("lastModifiedDate")
    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    @JsonProperty("lastModifiedDate")
    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @JsonProperty("contractNumber")
    public String getContractNumber() {
        return contractNumber;
    }

    @JsonProperty("contractNumber")
    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    @JsonProperty("commodityCategory")
    public String getCommodityCategory() {
        return commodityCategory;
    }

    @JsonProperty("commodityCategory")
    public void setCommodityCategory(String commodityCategory) {
        this.commodityCategory = commodityCategory;
    }

    @JsonProperty("commodityType")
    public String getCommodityType() {
        return commodityType;
    }

    @JsonProperty("commodityType")
    public void setCommodityType(String commodityType) {
        this.commodityType = commodityType;
    }

    @JsonProperty("brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonProperty("commodityPrice")
    public int getCommodityPrice() {
        return commodityPrice;
    }

    @JsonProperty("commodityPrice")
    public void setCommodityPrice(int commodityPrice) {
        this.commodityPrice = commodityPrice;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}