package com.example.estermelati.myapplication.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.estermelati.myapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyContractFragment extends Fragment {

    View view;
    @BindView(R.id.tv_my_contract) TextView myContract;

    public MyContractFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_my_contract, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        ButterKnife.bind(this, view);
    }
}
