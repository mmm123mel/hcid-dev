package com.example.estermelati.myapplication.model;

/**
 * Created by ester.melati on 27/06/2017.
 */

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "versionCode",
        "status",
        "message",
        "name",
        "email",
        "updateLink",
        "aboutUS",
        "phone",
        "contactUs",
        "themeColor",
        "contractHistories",
        "eligibility",
        "existingCustomer"
})
public class HomePageModel {

    @JsonProperty("versionCode")
    private String versionCode;
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("name")
    private String name;
    @JsonProperty("email")
    private String email;
    @JsonProperty("updateLink")
    private String updateLink;
    @JsonProperty("aboutUS")
    private String aboutUS;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("contactUs")
    private ContactUsModel contactUs;
    @JsonProperty("themeColor")
    private ThemeColorModel themeColor;
    @JsonProperty("contractHistories")
    private ContractHistoriesModel contractHistories;
    @JsonProperty("eligibility")
    private boolean eligibility;
    @JsonProperty("existingCustomer")
    private boolean existingCustomer;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("versionCode")
    public String getVersionCode(){
        return versionCode;
    }

    @JsonProperty("versionCode")
    public void setVersionCode(String versionCode){
        this.versionCode = versionCode;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("updateLink")
    public String getUpdateLink() {
        return updateLink;
    }

    @JsonProperty("updateLink")
    public void setUpdateLink(String updateLink) {
        this.updateLink = updateLink;
    }

    @JsonProperty("aboutUS")
    public String getAboutUs() {
        return aboutUS;
    }

    @JsonProperty("aboutUS")
    public void setAboutUs(String aboutUS) {
        this.aboutUS = aboutUS;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("contactUs")
    public ContactUsModel getContactUs() {
        return contactUs;
    }

    @JsonProperty("contactUs")
    public void setContactUs(ContactUsModel contactUs) {
        this.contactUs = contactUs;
    }

    @JsonProperty("themeColor")
    public ThemeColorModel getThemeColor() {
        return themeColor;
    }

    @JsonProperty("themeColor")
    public void setThemeColor(ThemeColorModel themeColor) {
        this.themeColor = themeColor;
    }

    @JsonProperty("contractHistories")
    public ContractHistoriesModel getContractHistories() {
        return contractHistories;
    }

    @JsonProperty("contractHistories")
    public void setContractHistories(ContractHistoriesModel contractHistories) {
        this.contractHistories = contractHistories;
    }

    @JsonProperty("eligibility")
    public boolean isEligibility() {
        return eligibility;
    }

    @JsonProperty("eligibility")
    public void setEligibility(boolean eligibility) {
        this.eligibility = eligibility;
    }

    @JsonProperty("existingCustomer")
    public boolean isExistingCustomer() {
        return existingCustomer;
    }

    @JsonProperty("existingCustomer")
    public void setExistingCustomer(boolean existingCustomer) {
        this.existingCustomer = existingCustomer;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}