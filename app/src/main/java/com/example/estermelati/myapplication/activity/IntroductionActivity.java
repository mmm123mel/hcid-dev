package com.example.estermelati.myapplication.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.RestApi;
import com.example.estermelati.myapplication.adapter.CustomPagerAdapter;
import com.example.estermelati.myapplication.fragment.CaraBayarCicilanFragment;
import com.example.estermelati.myapplication.fragment.CaraPengajuanFragment;
import com.example.estermelati.myapplication.fragment.ContactUsFragment;
import com.example.estermelati.myapplication.fragment.ContractDetailFragment;
import com.example.estermelati.myapplication.fragment.HomeFragment;
import com.example.estermelati.myapplication.fragment.InboxFragment;
import com.example.estermelati.myapplication.fragment.LokasiPembayaranFragment;
import com.example.estermelati.myapplication.fragment.MyContractFragment;
import com.example.estermelati.myapplication.fragment.PersonalDetailFragment;
import com.example.estermelati.myapplication.model.ContractHistoryModel;
import com.example.estermelati.myapplication.model.HomePageModel;
import com.example.estermelati.myapplication.response.PersonalDetailResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IntroductionActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.tv_toolbarTitle)
    TextView tv_toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
//    @BindView(R.id.tv_username)
    TextView tv_username;
    PersonalDetailResponse personalDetailResponse;
    HomePageModel homePageModel;
    CustomPagerAdapter customPagerAdapter;
    ArrayList<List<ContractHistoryModel>> contrHistoryData;
    FragmentManager fragmentManager;
    String pd_status, pd_message, fullname, motherName, pd_phone, email, birthdate,
            otherPhoneNumber, permanentAddress, contactAddress, bankAccount;
    String versionCode, contentType, accessToken, apiVersion, status, name, message, email_hp, updateLink,
            aboutUs, phone, contactUs_email, contactUs_phone, contactUs_fax,
            contractHistories_status, contractHistories_message, contractHistory_contractId,
            contractHistory_contractNumber, contractHistory_status, contractHistory_creationDate,
            contractHistory_nextPaymentDate, contractHistory_earliestUnpaidDueDate,
            contractHistory_productName, contractHistory_salesAgentName, contractHistory_posId,
            contractHistory_posName, contractHistory_posPhoneNumber, contractHistory_ferRequestStatus,
            contractHistory_ferRequestDate, contractHistory_totalCurrentInstallmentAmount,
            contractHistory_ferDueDate, contractHistory_indomaretVoucher,
            contractHistory_indomaretVoucherStatus, contractHistory_mpfInvoice,
            contractHistory_mpfInvoiceStatus, contractHistory_clientId, contractHistory_signDate,
            contractHistory_validTo, contractHistory_productCategory, commodityList_eid,
            commodityList_version, commodityList_createdDate, commodityList_lastModifiedDate,
            commodityList_contractNumber, commodityList_commodityCategory,
            commodityList_commodityType, commodityList_brand,  conServiceList;
    boolean eligibility, existingCustomer;
    int themeColor_red, themeColor_orange, themeColor_green, commodityList_commodityPrice, penalty,
            totalAmount, paidAmount, contractHistory_ferAmount, contractHistory_totalPaidDeb,
            contractHistory_totalDeb, contractHistory_totalUnpaidDeb,
            contractHistory_remainingUnpaidTerm, contractHistory_remainingUnpaidTermWithTolerance,
            contractHistory_cashPaymentAmount, contractHistory_downPaymentAmount,
            contractHistory_totalDpdWithTolerance, contractHistory_totalDpdWithoutTolerance,
            contractHistory_totalPenalty, contractHistory_term;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        ButterKnife.bind(this);
        initToolbar();
        accessToken = "Bearer " + getIntent().getStringExtra("accessToken");
        fragmentManager = getSupportFragmentManager();
        navigationView.setNavigationItemSelectedListener(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }
            @Override
            public void onDrawerOpened(View v){
                super.onDrawerOpened(v);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        View header = navigationView.getHeaderView(0);
        tv_username = (TextView)header.findViewById(R.id.tv_username);
        tv_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                loadPersonalDetail();
            }
        });
        startHomePage();
        loadPersonalDetails();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        tv_toolbarTitle.setText("My Home Credit");
        toolbar.setBackgroundColor(Color.TRANSPARENT);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_action_menu_white);
    }

    public void loadContractDetail(){
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new ContractDetailFragment())
                .addToBackStack(null).commit();
    }

    private void loadPersonalDetails() {
        personalDetailResponse = new PersonalDetailResponse();
        Log.d("accessToken pd", accessToken);
        apiVersion = "v2";
        RestApi restApi = RestApi.retrofit.create(RestApi.class);
        Call<PersonalDetailResponse> call = restApi.getPersonalDetails(accessToken, apiVersion);
        call.enqueue(new Callback<PersonalDetailResponse>() {
            @Override
            public void onResponse(Call<PersonalDetailResponse> call, Response<PersonalDetailResponse> response) {
                personalDetailResponse = response.body();
                Log.d("PD status", response.code() + " - " + response.message() +  " - " + response.body());
                pd_status = personalDetailResponse.getStatus();
                pd_message = personalDetailResponse.getMessage();
                fullname = personalDetailResponse.getPersonalDetails().getFullname();
                motherName = personalDetailResponse.getPersonalDetails().getMotherName();
                pd_phone = personalDetailResponse.getPersonalDetails().getPhone();
                email = personalDetailResponse.getPersonalDetails().getEmail();
                birthdate = personalDetailResponse.getPersonalDetails().getBirthdate();
                otherPhoneNumber = personalDetailResponse.getPersonalDetails().getOtherPhoneNumber();
                permanentAddress = personalDetailResponse.getPersonalDetails().getPermanentAddress();
                contactAddress = personalDetailResponse.getPersonalDetails().getContactAddress();
                bankAccount = personalDetailResponse.getPersonalDetails().getBankAccount();
                preparePdFullname();
                preparePdMotherName();
                preparePdPhone();
                preparePdEmail();
                preparePdBirthday();
                preparePdOtherPhoneNumber();
                preparePdPermanentAddress();
                preparePdContactAddress();
                preparePdBankAccount();
                tv_username.setText(fullname);
            }
            @Override
            public void onFailure(Call<PersonalDetailResponse> call, Throwable t) {
                Log.e("PersonalDetail fail", t.getLocalizedMessage());
            }
        });
    }

    public String preparePdBankAccount() {
        return bankAccount;
    }

    public String preparePdContactAddress() {
        return contactAddress;
    }

    public String preparePdPermanentAddress() {
        return permanentAddress;
    }

    public String preparePdOtherPhoneNumber() {
        return otherPhoneNumber;
    }

    public String preparePdBirthday() {
        return birthdate;
    }

    public String preparePdEmail() {
        return email;
    }

    public String preparePdPhone() {
        return pd_phone;
    }

    public String preparePdMotherName() {
        return motherName;
    }

    public String preparePdFullname() {
        return fullname;
    }

    public String prepareAccessToken(){
        return "Bearer " + getIntent().getStringExtra("accessToken");
    }

    public void loadInbox(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new InboxFragment())
                .addToBackStack(null).commit();
        tv_toolbarTitle.setText("Pesan Anda");
        toolbar.setBackgroundColor(Color.RED);
    }

    public void loadContactUs(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new ContactUsFragment()).commit();
        tv_toolbarTitle.setText("Hubungi Kami");
        toolbar.setBackgroundColor(Color.RED);
    }

    public void loadCaraPengajuan(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new CaraPengajuanFragment())
                .addToBackStack(null).commit();
        tv_toolbarTitle.setText("Cara Pengajuan");
        toolbar.setBackgroundColor(Color.RED);
    }

    public void loadMyContract(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new MyContractFragment())
                .addToBackStack(null).commit();
        tv_toolbarTitle.setText("Kontrak Saya");
        toolbar.setBackgroundColor(Color.RED);
    }

    public void loadPersonalDetail(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new PersonalDetailFragment())
                .addToBackStack(null).commit();
        tv_toolbarTitle.setText("Data Pribadi");
        toolbar.setBackgroundColor(Color.RED);
    }

    public void startHomePage(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new HomeFragment())
                .addToBackStack(null).commit();
        tv_toolbarTitle.setText("My Home Credit");
        toolbar.setBackgroundColor(Color.TRANSPARENT);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.logout){
            drawerLayout.closeDrawers();
//            loadPersonalDetail();
        } else if(id == R.id.inbox){
            drawerLayout.closeDrawers();
            loadInbox();
        } else if(id == R.id.home){
            drawerLayout.closeDrawers();
            startHomePage();
        } else if(id == R.id.contact_us){
            drawerLayout.closeDrawers();
            loadContactUs();
        } else if(id == R.id.help){
            drawerLayout.closeDrawers();
            loadCaraPengajuan();
        } else if(id == R.id.my_contract){
            drawerLayout.closeDrawers();
            loadMyContract();
        } else{
            drawerLayout.closeDrawers();
        }
        return true;
    }

    protected void onPause(){
        super.onPause();
    }
}
