package com.example.estermelati.myapplication.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andrognito.pinlockview.PinLockView;
import com.example.estermelati.myapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ester.melati on 05/07/2017.
 */

public class PinLockFragment extends Fragment {

    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    View view;
    FragmentManager fragmentManager;

    public PinLockFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = inflater.inflate(R.layout.pinlock_layout, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        ButterKnife.bind(this, view);
        fragmentManager = getActivity().getSupportFragmentManager();

    }

}
