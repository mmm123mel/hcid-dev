package com.example.estermelati.myapplication.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ester.melati on 19/06/2017.
 */

public class ExpandableListDataPump {

    public static HashMap<String, List<String>> getData(){
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();
        List<String> alfamart_indomaret = new ArrayList<String>();
        alfamart_indomaret.add("Alfamart, Indomaret, Alfamidi, Alfa Express, DanDan, Lawson, Ceriamart");
        alfamart_indomaret.add("Datangi KASIR, informasikan pembayaran...");

        List<String> kantor_pos = new ArrayList<String>();
        kantor_pos.add("Kunjungi KANTOR POS...");

        List<String> bca = new ArrayList<String>();
        bca.add("ATM BCA");
        bca.add("m-BCA (Mobile)");
        bca.add("Klik BCA");

        List<String> mandiri = new ArrayList<String>();
        mandiri.add("ATM Mandiri");
        mandiri.add("Internet Banking Mandiri");

        List<String> bri = new ArrayList<String>();
        bri.add("ATM BRI");
        bri.add("Internet Banking BRI");

        List<String> bni = new ArrayList<String>();
        bni.add("ATM BNI");
        bni.add("Internet Banking BNI");

        List<String> atm_bersama = new ArrayList<String>();
        atm_bersama.add("Pilih menu TRANSFER....");

        expandableListDetail.put("ALFAMART, INDOMARET", alfamart_indomaret);
        expandableListDetail.put("KANTOR POST", kantor_pos);
        expandableListDetail.put("BCA", bca);
        expandableListDetail.put("Mandiri", mandiri);
        expandableListDetail.put("BANK BRI", bri);
        expandableListDetail.put("BNI", bni);
        expandableListDetail.put("ATM Bersama", atm_bersama);

        return expandableListDetail;
    }
}
