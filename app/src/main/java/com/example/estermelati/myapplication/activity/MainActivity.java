package com.example.estermelati.myapplication.activity;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.support.design.widget.NavigationView;

import com.example.estermelati.myapplication.fragment.ContactUsFragment;
import com.example.estermelati.myapplication.fragment.HelpFragment;
import com.example.estermelati.myapplication.fragment.InboxFragment;
import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.fragment.MyContractFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.drawer) DrawerLayout drawerLayout;
    @BindView(R.id.main_navigation_view) NavigationView navigationView;
    private static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initNavigationDrawer();
    }

    public void loadInbox(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.main_frameContainer, new InboxFragment()).addToBackStack(null).commit();
        getSupportActionBar().setTitle("Pesan Anda");
    }

    public void loadHome(){
        Intent intent = new Intent(MainActivity.this, IntroductionActivity.class);
        startActivity(intent);
    }

    public void loadContactUs(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.main_frameContainer, new ContactUsFragment()).addToBackStack(null).commit();
        getSupportActionBar().setTitle("Hubungi Kami");
    }

    public void loadHelp(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.main_frameContainer, new HelpFragment()).addToBackStack(null).commit();
        getSupportActionBar().setTitle("Bantuan");
    }

    public void loadMyContract(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.main_frameContainer, new MyContractFragment()).addToBackStack(null).commit();
        getSupportActionBar().setTitle("Kontrak Saya");
    }

    public void logout(){
        drawerLayout.closeDrawers();
        Intent home = new Intent(MainActivity.this, IntroductionActivity.class);
        startActivity(home);
    }

    private void initNavigationDrawer() {
        fragmentManager = getSupportFragmentManager();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                switch (id){
                    case R.id.inbox:
                        loadInbox();
                        break;
                    case R.id.home:
                        loadHome();
                        break;
                    case R.id.contact_us:
                        loadContactUs();
                        break;
                    case R.id.help:
                        loadHelp();
                        break;
                    case R.id.my_contract:
                        loadMyContract();
                        break;
                    case R.id.logout:
                        logout();
                        break;
                }
                return true;
            }
        });

        View header = navigationView.getHeaderView(0);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v){
                super.onDrawerClosed(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }
}
