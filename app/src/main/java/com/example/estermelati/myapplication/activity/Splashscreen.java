package com.example.estermelati.myapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.BindView;

import com.example.estermelati.myapplication.R;
import com.facebook.shimmer.ShimmerFrameLayout;

public class Splashscreen extends AppCompatActivity {

    Context context;
    private static int SPLASH_TIME_OUT = 2000;
    @BindView(R.id.shimmer_view_container) ShimmerFrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);
        context = Splashscreen.this;
        container.startShimmerAnimation();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    protected void onPause(){
        container.stopShimmerAnimation();
        super.onPause();
    }
}
