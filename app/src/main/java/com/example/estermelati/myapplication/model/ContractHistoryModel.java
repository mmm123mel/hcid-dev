package com.example.estermelati.myapplication.model;

/**
 * Created by ester.melati on 27/06/2017.
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "contractId",
        "contractNumber",
        "status",
        "creationDate",
        "term",
        "nextPaymentDate",
        "earliestUnpaidDueDate",
        "remainingUnpaidTerm",
        "remainingUnpaidTermWithTolerance",
        "cashPaymentAmount",
        "donwPaymentAmount",
        "totalDpdWithTolerance",
        "totalDpdWithoutTolerance",
        "totalDeb",
        "totalUnpaidDeb",
        "totalPaidDeb",
        "totalPenalty",
        "productName",
        "salesAgentName",
        "posId",
        "posName",
        "posPhoneNumber",
        "ferRequestStatus",
        "ferRequestDate",
        "ferAmount",
        "totalCurrentInstallmentAmount",
        "ferDueDate",
        "indomaretVoucher",
        "indomaretVoucherStatus",
        "mpfInvoice",
        "mpfInvoiceStatus",
        "clientId",
        "signDate",
        "validTo",
        "productCategory",
        "commodityList",
        "conServiceList",
        "penalty",
        "totalAmount",
        "paidAmount"
})
public class ContractHistoryModel {

    @JsonProperty("contractId")
    private String contractId;
    @JsonProperty("contractNumber")
    private String contractNumber;
    @JsonProperty("status")
    private String status;
    @JsonProperty("creationDate")
    private String creationDate;
    @JsonProperty("term")
    private int term;
    @JsonProperty("nextPaymentDate")
    private String nextPaymentDate;
    @JsonProperty("earliestUnpaidDueDate")
    private String earliestUnpaidDueDate;
    @JsonProperty("remainingUnpaidTerm")
    private int remainingUnpaidTerm;
    @JsonProperty("remainingUnpaidTermWithTolerance")
    private int remainingUnpaidTermWithTolerance;
    @JsonProperty("cashPaymentAmount")
    private int cashPaymentAmount;
    @JsonProperty("donwPaymentAmount")
    private int donwPaymentAmount;
    @JsonProperty("totalDpdWithTolerance")
    private int totalDpdWithTolerance;
    @JsonProperty("totalDpdWithoutTolerance")
    private int totalDpdWithoutTolerance;
    @JsonProperty("totalDeb")
    private int totalDeb;
    @JsonProperty("totalUnpaidDeb")
    private int totalUnpaidDeb;
    @JsonProperty("totalPaidDeb")
    private int totalPaidDeb;
    @JsonProperty("totalPenalty")
    private int totalPenalty;
    @JsonProperty("productName")
    private String productName;
    @JsonProperty("salesAgentName")
    private String salesAgentName;
    @JsonProperty("posId")
    private String posId;
    @JsonProperty("posName")
    private String posName;
    @JsonProperty("posPhoneNumber")
    private String posPhoneNumber;
    @JsonProperty("ferRequestStatus")
    private String ferRequestStatus;
    @JsonProperty("ferRequestDate")
    private String ferRequestDate;
    @JsonProperty("ferAmount")
    private double ferAmount;
    @JsonProperty("totalCurrentInstallmentAmount")
    private int totalCurrentInstallmentAmount;
    @JsonProperty("ferDueDate")
    private String ferDueDate;
    @JsonProperty("indomaretVoucher")
    private String indomaretVoucher;
    @JsonProperty("indomaretVoucherStatus")
    private String indomaretVoucherStatus;
    @JsonProperty("mpfInvoice")
    private String mpfInvoice;
    @JsonProperty("mpfInvoiceStatus")
    private String mpfInvoiceStatus;
    @JsonProperty("clientId")
    private String clientId;
    @JsonProperty("signDate")
    private String signDate;
    @JsonProperty("validTo")
    private String validTo;
    @JsonProperty("productCategory")
    private String productCategory;
    @JsonProperty("commodityList")
    private List<CommodityList> commodityList = null;
    @JsonProperty("conServiceList")
    private List<ConServiceList> conServiceList = null;
    @JsonProperty("penalty")
    private int penalty;
    @JsonProperty("totalAmount")
    private int totalAmount;
    @JsonProperty("paidAmount")
    private int paidAmount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("contractId")
    public String getContractId() {
        return contractId;
    }

    @JsonProperty("contractId")
    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    @JsonProperty("contractNumber")
    public String getContractNumber() {
        return contractNumber;
    }

    @JsonProperty("contractNumber")
    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("creationDate")
    public String getCreationDate() {
        return creationDate;
    }

    @JsonProperty("creationDate")
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @JsonProperty("term")
    public int getTerm() {
        return term;
    }

    @JsonProperty("term")
    public void setTerm(int term) {
        this.term = term;
    }

    @JsonProperty("nextPaymentDate")
    public String getNextPaymentDate() {
        return nextPaymentDate;
    }

    @JsonProperty("nextPaymentDate")
    public void setNextPaymentDate(String nextPaymentDate) {
        this.nextPaymentDate = nextPaymentDate;
    }

    @JsonProperty("earliestUnpaidDueDate")
    public String getEarliestUnpaidDueDate() {
        return earliestUnpaidDueDate;
    }

    @JsonProperty("earliestUnpaidDueDate")
    public void setEarliestUnpaidDueDate(String earliestUnpaidDueDate) {
        this.earliestUnpaidDueDate = earliestUnpaidDueDate;
    }

    @JsonProperty("remainingUnpaidTerm")
    public int getRemainingUnpaidTerm() {
        return remainingUnpaidTerm;
    }

    @JsonProperty("remainingUnpaidTerm")
    public void setRemainingUnpaidTerm(int remainingUnpaidTerm) {
        this.remainingUnpaidTerm = remainingUnpaidTerm;
    }

    @JsonProperty("remainingUnpaidTermWithTolerance")
    public int getRemainingUnpaidTermWithTolerance() {
        return remainingUnpaidTermWithTolerance;
    }

    @JsonProperty("remainingUnpaidTermWithTolerance")
    public void setRemainingUnpaidTermWithTolerance(int remainingUnpaidTermWithTolerance) {
        this.remainingUnpaidTermWithTolerance = remainingUnpaidTermWithTolerance;
    }

    @JsonProperty("cashPaymentAmount")
    public int getCashPaymentAmount() {
        return cashPaymentAmount;
    }

    @JsonProperty("cashPaymentAmount")
    public void setCashPaymentAmount(int cashPaymentAmount) {
        this.cashPaymentAmount = cashPaymentAmount;
    }

    @JsonProperty("donwPaymentAmount")
    public int getDonwPaymentAmount() {
        return donwPaymentAmount;
    }

    @JsonProperty("donwPaymentAmount")
    public void setDonwPaymentAmount(int donwPaymentAmount) {
        this.donwPaymentAmount = donwPaymentAmount;
    }

    @JsonProperty("totalDpdWithTolerance")
    public int getTotalDpdWithTolerance() {
        return totalDpdWithTolerance;
    }

    @JsonProperty("totalDpdWithTolerance")
    public void setTotalDpdWithTolerance(int totalDpdWithTolerance) {
        this.totalDpdWithTolerance = totalDpdWithTolerance;
    }

    @JsonProperty("totalDpdWithoutTolerance")
    public int getTotalDpdWithoutTolerance() {
        return totalDpdWithoutTolerance;
    }

    @JsonProperty("totalDpdWithoutTolerance")
    public void setTotalDpdWithoutTolerance(int totalDpdWithoutTolerance) {
        this.totalDpdWithoutTolerance = totalDpdWithoutTolerance;
    }

    @JsonProperty("totalDeb")
    public int getTotalDeb() {
        return totalDeb;
    }

    @JsonProperty("totalDeb")
    public void setTotalDeb(int totalDeb) {
        this.totalDeb = totalDeb;
    }

    @JsonProperty("totalUnpaidDeb")
    public int getTotalUnpaidDeb() {
        return totalUnpaidDeb;
    }

    @JsonProperty("totalUnpaidDeb")
    public void setTotalUnpaidDeb(int totalUnpaidDeb) {
        this.totalUnpaidDeb = totalUnpaidDeb;
    }

    @JsonProperty("totalPaidDeb")
    public int getTotalPaidDeb() {
        return totalPaidDeb;
    }

    @JsonProperty("totalPaidDeb")
    public void setTotalPaidDeb(int totalPaidDeb) {
        this.totalPaidDeb = totalPaidDeb;
    }

    @JsonProperty("totalPenalty")
    public int getTotalPenalty() {
        return totalPenalty;
    }

    @JsonProperty("totalPenalty")
    public void setTotalPenalty(int totalPenalty) {
        this.totalPenalty = totalPenalty;
    }

    @JsonProperty("productName")
    public String getProductName() {
        return productName;
    }

    @JsonProperty("productName")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    @JsonProperty("salesAgentName")
    public String getSalesAgentName() {
        return salesAgentName;
    }

    @JsonProperty("salesAgentName")
    public void setSalesAgentName(String salesAgentName) {
        this.salesAgentName = salesAgentName;
    }

    @JsonProperty("posId")
    public String getPosId() {
        return posId;
    }

    @JsonProperty("posId")
    public void setPosId(String posId) {
        this.posId = posId;
    }

    @JsonProperty("posName")
    public String getPosName() {
        return posName;
    }

    @JsonProperty("posName")
    public void setPosName(String posName) {
        this.posName = posName;
    }

    @JsonProperty("posPhoneNumber")
    public String getPosPhoneNumber() {
        return posPhoneNumber;
    }

    @JsonProperty("posPhoneNumber")
    public void setPosPhoneNumber(String posPhoneNumber) {
        this.posPhoneNumber = posPhoneNumber;
    }

    @JsonProperty("ferRequestStatus")
    public String getFerRequestStatus() {
        return ferRequestStatus;
    }

    @JsonProperty("ferRequestStatus")
    public void setFerRequestStatus(String ferRequestStatus) {
        this.ferRequestStatus = ferRequestStatus;
    }

    @JsonProperty("ferRequestDate")
    public String getFerRequestDate() {
        return ferRequestDate;
    }

    @JsonProperty("ferRequestDate")
    public void setFerRequestDate(String ferRequestDate) {
        this.ferRequestDate = ferRequestDate;
    }

    @JsonProperty("ferAmount")
    public double getFerAmount() {
        return ferAmount;
    }

    @JsonProperty("ferAmount")
    public void setFerAmount(double ferAmount) {
        this.ferAmount = ferAmount;
    }

    @JsonProperty("totalCurrentInstallmentAmount")
    public int getTotalCurrentInstallmentAmount() {
        return totalCurrentInstallmentAmount;
    }

    @JsonProperty("totalCurrentInstallmentAmount")
    public void setTotalCurrentInstallmentAmount(int totalCurrentInstallmentAmount) {
        this.totalCurrentInstallmentAmount = totalCurrentInstallmentAmount;
    }

    @JsonProperty("ferDueDate")
    public String getFerDueDate() {
        return ferDueDate;
    }

    @JsonProperty("ferDueDate")
    public void setFerDueDate(String ferDueDate) {
        this.ferDueDate = ferDueDate;
    }

    @JsonProperty("indomaretVoucher")
    public String getIndomaretVoucher() {
        return indomaretVoucher;
    }

    @JsonProperty("indomaretVoucher")
    public void setIndomaretVoucher(String indomaretVoucher) {
        this.indomaretVoucher = indomaretVoucher;
    }

    @JsonProperty("indomaretVoucherStatus")
    public String getIndomaretVoucherStatus() {
        return indomaretVoucherStatus;
    }

    @JsonProperty("indomaretVoucherStatus")
    public void setIndomaretVoucherStatus(String indomaretVoucherStatus) {
        this.indomaretVoucherStatus = indomaretVoucherStatus;
    }

    @JsonProperty("mpfInvoice")
    public String getMpfInvoice() {
        return mpfInvoice;
    }

    @JsonProperty("mpfInvoice")
    public void setMpfInvoice(String mpfInvoice) {
        this.mpfInvoice = mpfInvoice;
    }

    @JsonProperty("mpfInvoiceStatus")
    public String getMpfInvoiceStatus() {
        return mpfInvoiceStatus;
    }

    @JsonProperty("mpfInvoiceStatus")
    public void setMpfInvoiceStatus(String mpfInvoiceStatus) {
        this.mpfInvoiceStatus = mpfInvoiceStatus;
    }

    @JsonProperty("clientId")
    public String getClientId() {
        return clientId;
    }

    @JsonProperty("clientId")
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @JsonProperty("signDate")
    public String getSignDate() {
        return signDate;
    }

    @JsonProperty("signDate")
    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    @JsonProperty("validTo")
    public String getValidTo() {
        return validTo;
    }

    @JsonProperty("validTo")
    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    @JsonProperty("productCategory")
    public String getProductCategory() {
        return productCategory;
    }

    @JsonProperty("productCategory")
    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    @JsonProperty("commodityList")
    public List<CommodityList> getCommodityList() {
        return commodityList;
    }

    @JsonProperty("commodityList")
    public void setCommodityList(List<CommodityList> commodityList) {
        this.commodityList = commodityList;
    }

    @JsonProperty("conServiceList")
    public List<ConServiceList> getConServiceList() {
        return conServiceList;
    }

    @JsonProperty("conServiceList")
    public void setConServiceList(List<ConServiceList> conServiceList) {
        this.conServiceList = conServiceList;
    }

    @JsonProperty("penalty")
    public int getPenalty() {
        return penalty;
    }

    @JsonProperty("penalty")
    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    @JsonProperty("totalAmount")
    public int getTotalAmount() {
        return totalAmount;
    }

    @JsonProperty("totalAmount")
    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    @JsonProperty("paidAmount")
    public int getPaidAmount() {
        return paidAmount;
    }

    @JsonProperty("paidAmount")
    public void setPaidAmount(int paidAmount) {
        this.paidAmount = paidAmount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}