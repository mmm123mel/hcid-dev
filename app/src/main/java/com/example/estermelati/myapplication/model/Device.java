package com.example.estermelati.myapplication.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "imei",
        "osVersion",
        "model",
        "manufacturer",
        "serialNumber",
        "career",
        "ram",
        "totalDiskMemory",
        "availableDiskMemory",
        "userId"
})
public class Device {

    @JsonProperty("imei")
    private String imei;
    @JsonProperty("osVersion")
    private String osVersion;
    @JsonProperty("model")
    private String model;
    @JsonProperty("manufacturer")
    private String manufacturer;
    @JsonProperty("serialNumber")
    private String serialNumber;
    @JsonProperty("career")
    private String career;
    @JsonProperty("ram")
    private String ram;
    @JsonProperty("totalDiskMemory")
    private String totalDiskMemory;
    @JsonProperty("availableDiskMemory")
    private String availableDiskMemory;
    @JsonProperty("userId")
    private String userId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("imei")
    public String getImei() {
        return imei;
    }

    @JsonProperty("imei")
    public void setImei(String imei) {
        this.imei = imei;
    }

    @JsonProperty("osVersion")
    public String getOsVersion() {
        return osVersion;
    }

    @JsonProperty("osVersion")
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    @JsonProperty("model")
    public String getModel() {
        return model;
    }

    @JsonProperty("model")
    public void setModel(String model) {
        this.model = model;
    }

    @JsonProperty("manufacturer")
    public String getManufacturer() {
        return manufacturer;
    }

    @JsonProperty("manufacturer")
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @JsonProperty("serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    @JsonProperty("serialNumber")
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty("career")
    public String getCareer() {
        return career;
    }

    @JsonProperty("career")
    public void setCareer(String career) {
        this.career = career;
    }

    @JsonProperty("ram")
    public String getRam() {
        return ram;
    }

    @JsonProperty("ram")
    public void setRam(String ram) {
        this.ram = ram;
    }

    @JsonProperty("totalDiskMemory")
    public String getTotalDiskMemory() {
        return totalDiskMemory;
    }

    @JsonProperty("totalDiskMemory")
    public void setTotalDiskMemory(String totalDiskMemory) {
        this.totalDiskMemory = totalDiskMemory;
    }

    @JsonProperty("availableDiskMemory")
    public String getAvailableDiskMemory() {
        return availableDiskMemory;
    }

    @JsonProperty("availableDiskMemory")
    public void setAvailableDiskMemory(String availableDiskMemory) {
        this.availableDiskMemory = availableDiskMemory;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}