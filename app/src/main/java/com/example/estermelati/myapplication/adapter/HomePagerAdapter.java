package com.example.estermelati.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.activity.IntroductionActivity;
import com.example.estermelati.myapplication.fragment.ContractDetailFragment;
import com.example.estermelati.myapplication.model.ContractHistoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ester.melati on 06/07/2017.
 */

public class HomePagerAdapter extends PagerAdapter {
    private ArrayList<ContractHistoryModel> contractHistory = new ArrayList<>();
    private Context mContext;
    LayoutInflater layoutInflater;
    @BindView(R.id.tv_contract_number)
    TextView tv_contract_number;
    @BindView(R.id.tv_amount)
    TextView tv_amount;
    @BindView(R.id.tv_day)
    TextView tv_day;
    String contractNumber, nextPaymentDate, cashPaymentAmount;
    private FragmentManager fragmentManager;

    public HomePagerAdapter(Context context, ArrayList<ContractHistoryModel> contractHistory) {
        this.mContext = context;
        this.contractHistory = contractHistory;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){
        Log.d("HomePagerAdapter", "contractHistory size: " + String.valueOf(contractHistory.size()));
        View view = ((Activity)mContext).getLayoutInflater().inflate(R.layout.contract_history_layout, container, false);
        ButterKnife.bind(this, view);
        contractNumber = contractHistory.get(position).getContractNumber();
        nextPaymentDate = contractHistory.get(position).getNextPaymentDate();
        cashPaymentAmount = "Rp " + String.valueOf(contractHistory.get(position).getCashPaymentAmount());
        tv_contract_number.setText("No. Kontrak:\n" + contractNumber);
        tv_amount.setText(cashPaymentAmount);
        tv_day.setText("Rekomentasi Tanggal Pembayaran:\n" + nextPaymentDate);

        Log.d("HomePagerAdapter", contractNumber);
        Log.d("HomePagerAdapter", nextPaymentDate);
        Log.d("HomePagerAdapter", cashPaymentAmount);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = (Activity) mContext;
                ((IntroductionActivity)activity).loadContractDetail();
            }
        });
        container.addView(view);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object view){
        container.removeView((View) view);
    }

    @Override
    public int getCount() {
        return contractHistory.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
