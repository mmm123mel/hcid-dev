package com.example.estermelati.myapplication.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.estermelati.myapplication.model.CaraPengajuanModelObject;

/**
 * Created by ester.melati on 22/06/2017.
 */

public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;

    public CustomPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position){
        CaraPengajuanModelObject modelObject = CaraPengajuanModelObject.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view){
        collection.removeView((View) view);
    }

    @Override
    public int getCount(){
        return CaraPengajuanModelObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return view == object;
    }

}
