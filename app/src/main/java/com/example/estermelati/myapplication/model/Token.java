package com.example.estermelati.myapplication.model;

import java.util.HashMap;
import java.util.Map;

public class Token {

    private String username;
    private String accessToken;
    private String refreshToken;
    private int expired;
    private String tokenCreated;
    private String tokenExpired;
    private boolean isDummy;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Token() {
    }

    /**
     *
     * @param username
     * @param expired
     * @param accessToken
     * @param tokenExpired
     * @param tokenCreated
     * @param refreshToken
     * @param isDummy
     */
    public Token(String username, String accessToken, String refreshToken, int expired, String tokenCreated, String tokenExpired, boolean isDummy) {
        super();
        this.username = username;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expired = expired;
        this.tokenCreated = tokenCreated;
        this.tokenExpired = tokenExpired;
        this.isDummy = isDummy;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public int getExpired() {
        return expired;
    }

    public void setExpired(int expired) {
        this.expired = expired;
    }

    public String getTokenCreated() {
        return tokenCreated;
    }

    public void setTokenCreated(String tokenCreated) {
        this.tokenCreated = tokenCreated;
    }

    public String getTokenExpired() {
        return tokenExpired;
    }

    public void setTokenExpired(String tokenExpired) {
        this.tokenExpired = tokenExpired;
    }

    public boolean isDummy() {
        return isDummy;
    }

    public void setDummy(boolean dummy) {
        isDummy = dummy;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}