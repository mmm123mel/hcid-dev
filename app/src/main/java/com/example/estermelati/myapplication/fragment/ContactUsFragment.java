package com.example.estermelati.myapplication.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.estermelati.myapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactUsFragment extends Fragment {
    private static View view;
    private FragmentManager fragmentManager;
    @BindView(R.id.tv_contact_us)
    TextView contact_us;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        ButterKnife.bind(this, view);
        fragmentManager = getActivity().getSupportFragmentManager();
    }
}
