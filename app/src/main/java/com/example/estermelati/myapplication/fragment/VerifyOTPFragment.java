package com.example.estermelati.myapplication.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.RestApi;
import com.example.estermelati.myapplication.activity.IntroductionActivity;
import com.example.estermelati.myapplication.activity.SignUpActivity;
import com.example.estermelati.myapplication.model.Device;
import com.example.estermelati.myapplication.model.RegisterModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ester.melati on 04/07/2017.
 */

public class VerifyOTPFragment extends Fragment {

    private static View view;
    private FragmentManager fragmentManager;
    RegisterModel registerModel;
    Device device;
    String otpId, otpCode, email, userId, phone, pin, loginType, dateOfBirth, deviceId, imei,
            osVersion, model, manufacturer, serialNumber, career, ram, totalDiskMemory, availableDiskMemory;
    ProgressDialog progressDialog;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.btnBack)
    TextView btnBack;
    @BindView(R.id.tv_lanjut)
    TextView tv_lanjut;
    @BindView(R.id.tv_hintOTPVer)
    TextView tv_hintOTPVer;
    @BindView(R.id.tv_timesUp)
    TextView tv_timesUp;
    @BindView(R.id.et_PIN)
    EditText et_PIN;
    @BindView(R.id.resendSMS)
    Button resendSMS;
    @BindView(R.id.tv_toolbarTitle)
    TextView tv_toolbarTitle;

    public VerifyOTPFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_verify_otp, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.btnBack) void backToRegisterForm(){

    }

    @OnClick(R.id.tv_lanjut) void register() {
        registerModel = new RegisterModel();
        fragmentManager = getActivity().getSupportFragmentManager();
        retrieveData();
        device = new Device();
        device.setImei(imei);
        device.setOsVersion(osVersion);
        device.setModel(model);
        device.setManufacturer(manufacturer);
        device.setSerialNumber(serialNumber);
        device.setCareer(career);
        device.setRam(ram);
        device.setTotalDiskMemory(totalDiskMemory);
        device.setAvailableDiskMemory(availableDiskMemory);
        device.setUserId(userId);
        registerModel.setOtpId(otpId);
        registerModel.setOtpCode(otpCode);
        registerModel.setEmail(email);
        registerModel.setUserId(userId);
        registerModel.setPhone(phone);
        registerModel.setPin(pin);
        registerModel.setLoginType(loginType);
        registerModel.setDateOfBirth(dateOfBirth);
        registerModel.setDevice(device);

        RestApi restApi = RestApi.retrofit.create(RestApi.class);
        Call<RegisterModel> call = restApi.register(registerModel);
        call.enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                registerModel = response.body();
                String status = registerModel.getStatus();
                String message = registerModel.getMessage();
                progressDialog.dismiss();
                loadHome();
            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                Log.e("ERROR", t.getMessage());
                Toast.makeText(getActivity(), "Gagal login", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadHome() {
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new HomeFragment())
                .addToBackStack("f_personalDetail").commit();
        tv_toolbarTitle.setText("My Home Credit");
        toolbar.setBackgroundColor(Color.RED);
    }

    public void retrieveData() {
        SignUpActivity activity = (SignUpActivity) getActivity();

    }
}
