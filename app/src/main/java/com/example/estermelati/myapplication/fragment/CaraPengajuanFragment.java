package com.example.estermelati.myapplication.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.estermelati.myapplication.R;
import com.example.estermelati.myapplication.adapter.CustomPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CaraPengajuanFragment extends Fragment {

    View view;
    @BindView(R.id.vp)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    CustomPagerAdapter customPagerAdapter;

    public CaraPengajuanFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_cara_pengajuan, container, false);
        ButterKnife.bind(this, view);
        setViewPager(viewPager);
        return view;
    }

    private void setViewPager(ViewPager viewPager) {
        customPagerAdapter = new CustomPagerAdapter(getActivity());
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

}
