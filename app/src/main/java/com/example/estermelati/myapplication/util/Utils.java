package com.example.estermelati.myapplication.util;

/**
 * Created by ester.melati on 15/06/2017.
 */

public class Utils {

    //Email Validation pattern
    public static final String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

    //Fragments Tags
    public static final String LoginFragment = "LoginFragment";
    public static final String SignUpFragment = "SignUpFragment";
    public static final String ForgotPasswordFragment = "ForgotPasswordFragment";
    public static final String CaraBayarCicilanFragment = "CaraBayarCicilanFragment";
    public static final String CaraPengajuanFragment = "CaraPengajuanFragment";
    public static final String ContactUsFragment = "ContactUsFragment";
    public static final String HelpFragment = "HelpFragment";
    public static final String HomeFragment = "HomeFragment";
    public static final String InboxFragment = "InboxFragment";
    public static final String LokasiPembayaranFragment = "LokasiPembayaranFragment";
    public static final String MyContractFragment = "MyContractFragment";

}